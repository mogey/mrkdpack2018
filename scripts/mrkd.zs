import crafttweaker.item.IIngredient;
import crafttweaker.item.IItemStack;
import crafttweaker.oredict.IOreDict;
import crafttweaker.oredict.IOreDictEntry;


/* This script is to create a all-encompassing Akashic Tome that we will give to players on spawn.  */


var allTome = <akashictome:tome>.withTag({"akashictome:data": {
tconstruct: {id: "tconstruct:book", Count: 1 as byte, tag: {"akashictome:definedMod": "tconstruct"}, Damage: 0 as short},
actuallyadditions: {id: "actuallyadditions:item_booklet", Count: 1 as byte, tag: {"akashictome:definedMod": "actuallyadditions"}, Damage: 0 as short},
armorplus: {id: "armorplus:book", Count: 1 as byte, tag: {"akashictome:definedMod": "armorplus"}, Damage: 0 as short},
guideapi1: {id: "guideapi:cyclicmagic-guide", Count: 1 as byte, tag: {"akashictome:definedMod": "guideapi1"}, Damage: 0 as short},
guideapi2: {id: "guideapi:vampirism-guide", Count: 1 as byte, tag: {"akashictome:definedMod": "guideapi2"}, Damage: 0 as short},
guideapi3: {id: "guideapi:woot-guide", Count: 1 as byte, tag: {"akashictome:definedMod": "guideapi3"}, Damage: 0 as short},
openblocks: {id: "openblocks:info_book", Count: 1 as byte, tag: {"akashictome:definedMod": "openblocks"}, Damage: 0 as short},
opencomputers: {id: "opencomputers:tool", Count: 1 as byte, tag: {"akashictome:definedMod": "opencomputers"}, Damage: 4 as short},
theoneprobe: {id: "theoneprobe:probenote", Count: 1 as byte, tag: {"akashictome:definedMod": "theoneprobe"}, Damage: 0 as short},
astralsorcery: {id: "astralsorcery:itemjournal", Count: 1 as byte, tag: {"akashictome:definedMod": "astralsorcery"}, Damage: 0 as short},
immersiveengineering: {id: "immersiveenginneering:tool", Count: 1 as byte, tag: {"akashictome:definedMod": "immersiveenginneering"}, Damage: 0 as short},
extrautils2: {id: "extrautils2:book", Count: 1 as byte, tag: {"akashictome:definedMod": "extrautils2"}, Damage: 0 as short},
industrialforegoing: {id: "industrialforegoing:book_manual", Count: 1 as byte, tag: {"akashictome:definedMod": "industrialforegoing"}, Damage: 0 as short},
rftools: {id: "rftools:rftools_manual", Count: 1 as byte, tag: {"akashictome:definedMod": "rftools"}, Damage: 0 as short},
rftools2: {id: "rftools:rftools_shape_manual", Count: 1 as byte, tag: {"akashictome:definedMod": "rftools1"}, Damage: 0 as short},
rftoolscontrol: {id: "rftoolscontrol:rftoolscontrol_manual", Count: 1 as byte, tag: {"akashictome:definedMod": "rftoolscontrol"}, Damage: 0 as short},
rftoolsdim: {id: "rftoolsdim:rftoolsdim_manual", Count: 1 as byte, tag: {"akashictome:definedMod": "rftoolsdim"}, Damage: 0 as short},
xnet: {id: "xnet:xnet_manual", Count: 1 as byte, tag: {"akashictome:definedMod": "xnet"}, Damage: 0 as short},
totemic: {id: "totemic:totempedia", Count: 1 as byte, tag: {"akashictome:definedMod": "totemic"}, Damage: 0 as short},
abyssalcraft: {id: "abyssalcraft:necronomicon", Count: 1 as byte, tag: {"akashictome:definedMod": "abyssalcraft"}, Damage: 0 as short},
botania: {id: "botania:lexicon", Count: 1 as byte, tag: {"akashictome:definedMod": "botania"}, Damage: 0 as short}
}});



/* Adding metadata */
allTome.addTooltip(format.blue("Contains all instructional books that mods have added."));
mods.jei.JEI.addDescription(allTome, "This pack contains more books than you can shake a stick at. So we added a recipe to change a stick into a book with all of them!");
/* Adding to JEI. */
mods.jei.JEI.addItem(allTome);

/* Adding recipe in case player loses it. */
recipes.addShapeless("mRKD Tome", allTome, [<minecraft:stick>]);

/*Adding crafting for stairs and slabs of different types*/
recipes.addShaped(<minecraft:chest_minecart>, [[null, null, null],[<minecraft:iron_ingot>, <quark:custom_chest_trap:3>, <minecraft:iron_ingot>], [<minecraft:iron_ingot>, <minecraft:iron_ingot>, <minecraft:iron_ingot>]]);
recipes.addShaped(<minecraft:chest_minecart>, [[null, null, null],[<minecraft:iron_ingot>, <quark:custom_chest_trap:2>, <minecraft:iron_ingot>], [<minecraft:iron_ingot>, <minecraft:iron_ingot>, <minecraft:iron_ingot>]]);
recipes.addShaped(<minecraft:chest_minecart>, [[null, null, null],[<minecraft:iron_ingot>, <quark:custom_chest_trap:1>, <minecraft:iron_ingot>], [<minecraft:iron_ingot>, <minecraft:iron_ingot>, <minecraft:iron_ingot>]]);
recipes.addShaped(<minecraft:chest_minecart>, [[null, null, null],[<minecraft:iron_ingot>, <quark:custom_chest_trap>, <minecraft:iron_ingot>], [<minecraft:iron_ingot>, <minecraft:iron_ingot>, <minecraft:iron_ingot>]]);
recipes.addShaped(<minecraft:chest_minecart>, [[null, null, null],[<minecraft:iron_ingot>, <quark:custom_chest:4>, <minecraft:iron_ingot>], [<minecraft:iron_ingot>, <minecraft:iron_ingot>, <minecraft:iron_ingot>]]);
recipes.addShaped(<minecraft:chest_minecart>, [[null, null, null],[<minecraft:iron_ingot>, <quark:custom_chest:3>, <minecraft:iron_ingot>], [<minecraft:iron_ingot>, <minecraft:iron_ingot>, <minecraft:iron_ingot>]]);
recipes.addShaped(<minecraft:chest_minecart>, [[null, null, null],[<minecraft:iron_ingot>, <quark:custom_chest:2>, <minecraft:iron_ingot>], [<minecraft:iron_ingot>, <minecraft:iron_ingot>, <minecraft:iron_ingot>]]);
recipes.addShaped(<minecraft:chest_minecart>, [[null, null, null],[<minecraft:iron_ingot>, <quark:custom_chest:1>, <minecraft:iron_ingot>], [<minecraft:iron_ingot>, <minecraft:iron_ingot>, <minecraft:iron_ingot>]]);
recipes.addShaped(<minecraft:chest_minecart>, [[null, null, null],[<minecraft:iron_ingot>, <quark:custom_chest>, <minecraft:iron_ingot>], [<minecraft:iron_ingot>, <minecraft:iron_ingot>, <minecraft:iron_ingot>]]);
recipes.addShaped(<minecraft:chest_minecart>, [[null, null, null],[<minecraft:iron_ingot>, <quark:custom_chest_trap:4>, <minecraft:iron_ingot>], [<minecraft:iron_ingot>, <minecraft:iron_ingot>, <minecraft:iron_ingot>]]);
recipes.addShaped(<minecraft:chest_minecart>, [[null, null, null],[<minecraft:iron_ingot>, <bibliocraft:framedchest:6>, <minecraft:iron_ingot>], [<minecraft:iron_ingot>, <minecraft:iron_ingot>, <minecraft:iron_ingot>]]);
recipes.addShaped(<minecraft:chest_minecart>, [[null, null, null],[<minecraft:iron_ingot>, <bibliocraft:framedchest>, <minecraft:iron_ingot>], [<minecraft:iron_ingot>, <minecraft:iron_ingot>, <minecraft:iron_ingot>]]);
recipes.addShaped(<minecraft:chest_minecart>, [[null, null, null],[<minecraft:iron_ingot>, <bibliocraft:framedchest:5>, <minecraft:iron_ingot>], [<minecraft:iron_ingot>, <minecraft:iron_ingot>, <minecraft:iron_ingot>]]);
recipes.addShaped(<minecraft:chest_minecart>, [[null, null, null],[<minecraft:iron_ingot>, <bibliocraft:framedchest:4>, <minecraft:iron_ingot>], [<minecraft:iron_ingot>, <minecraft:iron_ingot>, <minecraft:iron_ingot>]]);
recipes.addShaped(<minecraft:chest_minecart>, [[null, null, null],[<minecraft:iron_ingot>, <bibliocraft:framedchest:3>, <minecraft:iron_ingot>], [<minecraft:iron_ingot>, <minecraft:iron_ingot>, <minecraft:iron_ingot>]]);
recipes.addShaped(<minecraft:chest_minecart>, [[null, null, null],[<minecraft:iron_ingot>, <bibliocraft:framedchest:2>, <minecraft:iron_ingot>], [<minecraft:iron_ingot>, <minecraft:iron_ingot>, <minecraft:iron_ingot>]]);
recipes.addShaped(<minecraft:chest_minecart>, [[null, null, null],[<minecraft:iron_ingot>, <bibliocraft:framedchest:1>, <minecraft:iron_ingot>], [<minecraft:iron_ingot>, <minecraft:iron_ingot>, <minecraft:iron_ingot>]]);
recipes.addShapedMirrored(<quark:stone_marble_stairs> * 8, [[<chisel:marble2:7>, null, null],[<chisel:marble2:7>, <chisel:marble2:7>, null], [<chisel:marble2:7>, <chisel:marble2:7>, <chisel:marble2:7>]]);
recipes.addShapedMirrored(<quark:stone_marble_slab> * 6, [[null, null, null],[<chisel:marble2:7>, <chisel:marble2:7>, <chisel:marble2:7>], [null, null, null]]);
recipes.addShapeless(<quark:stone_limestone_slab> * 6, [<magneticraft:limestone>,<magneticraft:limestone>,<magneticraft:limestone>]);
recipes.addShapeless(<quark:stone_limestone_slab> * 6, [<chisel:limestone2:7>,<chisel:limestone2:7>,<chisel:limestone2:7>]);
recipes.addShapeless(<quark:limestone:1> * 4, [<magneticraft:limestone>,<magneticraft:limestone>,<magneticraft:limestone>,<magneticraft:limestone>]);
recipes.addShapeless(<quark:limestone:1> * 4, [<chisel:limestone2:7>,<chisel:limestone2:7>,<chisel:limestone2:7>,<chisel:limestone2:7>]);
recipes.addShapedMirrored(<quark:stone_limestone_stairs> * 8, [[<magneticraft:limestone>, null, null],[<magneticraft:limestone>, <magneticraft:limestone>, null], [<magneticraft:limestone>, <magneticraft:limestone>, <magneticraft:limestone>]]);
recipes.addShapedMirrored(<quark:stone_limestone_stairs> * 8, [[<chisel:limestone2:7>, null, null],[<chisel:limestone2:7>, <chisel:limestone2:7>, null], [<chisel:limestone2:7>, <chisel:limestone2:7>, <chisel:limestone2:7>]]);
//fixing iron lattice conflict
recipes.removeShaped(<rustic:iron_lattice> * 16, [[null, <minecraft:iron_ingot>, null],[<minecraft:iron_ingot>, <minecraft:iron_ingot>, <minecraft:iron_ingot>], [null, <minecraft:iron_ingot>, null]]);
recipes.addShaped(<rustic:iron_lattice> * 16, [[<minecraft:iron_ingot>, null, <minecraft:iron_ingot>],[<minecraft:iron_ingot>, null, <minecraft:iron_ingot>], [null, <minecraft:iron_ingot>, null]]);
/* Addding to starting inventory. */
mods.initialinventory.InvHandler.addStartingItem(allTome);
mods.initialinventory.InvHandler.addStartingItem(<minecraft:stone_pickaxe>);
mods.initialinventory.InvHandler.addStartingItem(<minecraft:stone_shovel>);
mods.initialinventory.InvHandler.addStartingItem(<minecraft:stone_sword>);
mods.initialinventory.InvHandler.addStartingItem(<minecraft:stone_axe>);
mods.initialinventory.InvHandler.addStartingItem(<minecraft:bread> * 16);